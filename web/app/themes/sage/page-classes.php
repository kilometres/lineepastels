<?php
/*
Template Name: Classes
*/
?>

<?php query_posts( array( 'posts_per_page' => 100,
                          'post_type' => array( 'class' ),
                          'paged' => get_query_var( 'paged' ),
                          'order' => 'ASC',
                          'orderby' => 'meta_value',
                          'meta_key' => 'start_date' ) ); ?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/class', 'page'); ?>
<?php endwhile; ?>
