<?php $date = DateTime::createFromFormat('Ymd', get_field('start_date')); ?>

<article class="class shadow-1">
  <div class="row no-margin">
    <div class="row-same-height">
      <div class="col-md-8 col-md-height healthy-padding">
        <header>
          <span class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
        </header>
        <div class="entry-summary head-room"><?php the_field('class_description'); ?></div>
      </div>
      <div class="col-md-4 col-md-height healthy-padding left-divider col-middle center-text">
        <span class="class-date"><?php echo $date->format('M d, Y'); ?></span>
        <br/>
        <span class="class-times head-room"><?php the_field('days_and_hours'); ?></span>
      </div>
    </div>
  </div>
</article>
