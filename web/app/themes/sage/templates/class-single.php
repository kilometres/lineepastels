<?php while (have_posts()) : the_post(); ?>
  <?php $date = DateTime::createFromFormat('Ymd', get_field('start_date')); ?>
  <article class="class class-single">
    <div class="row no-margin">
      <div class="row-same-height">
        <div class="col-md-12 col-md-height healthy-padding col-middle center-text">
          <header>
            <span class="entry-title larger"><?php the_title(); ?></span>
          </header>
        </div>
      </div>
    </div>
    <div class="row no-margin">
      <div class="col-md-12">
        <div class="entry-summary head-room"><?php the_field('class_description'); ?></div>
      </div>
    </div>
    <br><br>

    <?php
      $location = get_field('class_location');
      if( !empty($location) ):
    ?>
    <div class="row no-margin">
      <div class="col-md-12">
        <iframe width="100%" height="auto" frameborder="0" style="border:0; min-height: 300px" src="https://www.google.com/maps/embed/v1/place?q=<?php echo urlencode($location['address']); ?>&key=AIzaSyC__HE2KxvqhnfTLAn0pAoyrmki-bVp_mw"></iframe>
      </div>
    </div>
    <?php endif; ?>
    <br>
    <div class="row no-margin">
      <div class="col-md-4 healthy-padding center-text">
        <div class="simple-box headroom">
          <span class="class-date"><?php echo $date->format('M d, Y'); ?></span>
          <br/>
          <span class="class-times head-room"><?php the_field('days_and_hours'); ?></span>
        </div>
      </div>
      <div class="col-md-8">
        <div class="simple-box md-extra">
          <header><span class="entry-title larger">Learn more</span></header>
          <br>
          <?php echo do_shortcode('[contact_bank form_id=2]'); ?>
        </div>
      </div>
    </div>
  </article>
<?php endwhile; ?>
