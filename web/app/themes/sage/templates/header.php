<?php use Roots\Sage\Nav\NavWalker; ?>

<header class="banner navbar navbar-center navbar-default navbar-static-top" role="banner">
  <div class="container">
      <div class="navbar-header center-text">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
          Menu
        </button>
        <div id="brand-holder" class="visible-xs small" data-toggle="collapse" data-target=".navbar-collapse">
          <a class="navbar-brand"><img id="linee-logo" src="<?= get_template_directory_uri(); ?>/dist/images/sig.jpg"></a>
        </div>
      </div>

      <nav class="collapse navbar-collapse" role="navigation">
        <div id="menu-block">
          <?php
          if (has_nav_menu('left_navigation')) :
            wp_nav_menu(['theme_location' => 'left_navigation', 'walker' => new NavWalker(), 'menu_class' => 'nav navbar-nav']);
          endif;
          ?>
          <div id="brand-holder" class="hidden-xs">
            <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>"><img id="linee-logo" src="<?= get_template_directory_uri(); ?>/dist/images/sig.jpg"></a>
          </div>
          <?php if (has_nav_menu('right_navigation')) :
            wp_nav_menu(['theme_location' => 'right_navigation', 'walker' => new NavWalker(), 'menu_class' => 'nav navbar-nav']);
          endif;
          ?>
        </div>
      </nav>
  </div>
</header>
